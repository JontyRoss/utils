package graphqlex

import (
	"regexp"
	"strconv"

	"bitbucket.org/ksenaGroup/utils/logger"
	"github.com/graphql-go/graphql"
	"github.com/graphql-go/graphql/language/ast"
)

type Repetition struct {
	//ID         uint   `json:"id"`
	Freq       string `json:"freq"`
	ByDay      string `json:"byDay"`
	ByMonthDay int    `json:"byMonthDay"`
	Interval   int    `json:"interval"`
}

var RepetitionRule = graphql.NewScalar(graphql.ScalarConfig{
	Name: "RepetitionRule",
	Description: "The `RepetitionRule` scalar type represents a RepetitionRule." +
		" The RepetitionPeriod is in the iCalendar format",
	Serialize:  serializeRepetitionRule,
	ParseValue: unserializeRepetitionRule,
	ParseLiteral: func(valueAST ast.Value) interface{} {
		switch valueAST := valueAST.(type) {
		case *ast.StringValue:
			return unserializeRepetitionRule(valueAST.Value)
		}
		return nil
	},
})

//Takes duration and converts to string in the iCalendar format
//Examples:
//FREQ=DAILY;INTERVAL=1
//FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR;INTERVAL=1
//FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR,SA;INTERVAL=1
//FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR,SU;INTERVAL=1
//FREQ=MONTHLY;BYMONTHDAY=1;INTERVAL=1
//FREQ=MONTHLY;BYMONTHDAY=1;INTERVAL=3
//FREQ=MONTHLY;BYMONTHDAY=1;INTERVAL=6
func serializeRepetitionRule(value interface{}) interface{} {
	switch value := value.(type) {
	case Repetition:
		rRuleString := "FREQ=" + value.Freq + ";"
		switch value.Freq {
		case "WEEKLY":
			rRuleString += "BYDAY=" + value.ByDay + ";"
			break
		case "MONTHLY":
			rRuleString += "BYMONTHDAY=" + strconv.Itoa(value.ByMonthDay) + ";"
			break
		}
		rRuleString += "INTERVAL=" + strconv.Itoa(value.Interval)
		return rRuleString
	case map[string]interface{}:
		var rRuleString string
		if freq, freqOk := value["freq"].(string); freqOk {
			rRuleString += "FREQ=" + freq + ";"
			switch freq {
			case "WEEKLY":
				if byDay, byDayOk := value["byDay"].(string); byDayOk {
					rRuleString += "BYDAY=" + byDay + ";"
				} else {
					logger.Errorf("Serialize RepetitionRule failed as byDay could not be found, value: %+v", value)
					return nil
				}
				break
			case "MONTHLY":
				if byMonthDay, byMonthDayOk := value["byMonthDay"].(float64); byMonthDayOk {
					rRuleString += "BYMONTHDAY=" + strconv.Itoa(int(byMonthDay)) + ";"
				} else {
					logger.Errorf("Serialize RepetitionRule failed as byMonthDay could not be found, value: %+v", value)
					return nil
				}
				break
			}

		} else {
			logger.Errorf("Serialize RepetitionRule failed as freq could not be found, value: %+v", value)
			return nil
		}

		if interval, intervalOk := value["interval"].(float64); intervalOk {
			rRuleString += "INTERVAL=" + strconv.Itoa(int(interval))
		} else {
			logger.Errorf("Serialize RepetitionRule failed as interval could not be found, value: %+v", value)
			return nil
		}

		return rRuleString
	default:
		logger.Errorf("Serialize RepetitionRule failed as the input was of the wrong type: %+v", value)
		return nil
	}
}

//Converts repetition rule input string to Repetition struct
//This is called when converting from the incoming graphql and placing the values in the args map[string]interface
func unserializeRepetitionRule(value interface{}) interface{} {
	switch value := value.(type) {
	case string:
		re := regexp.MustCompile(`FREQ=(DAILY|WEEKLY|MONTHLY);((BYDAY|BYMONTHDAY)=(.+);)*INTERVAL=(\d+)+`) //[^,]+
		matches := re.FindStringSubmatch(value)
		if len(matches) == 0 {
			logger.Error("Repetition Rule format incorrect")
			return nil
		}
		//Match[0] is the entire Rrule. Subsequent indices are the subselections:
		var repetition Repetition
		repetition.Freq = matches[1]
		switch repetition.Freq {
		case "DAILY":
			break
		case "WEEKLY":
			if matches[3] != "BYDAY" {
				logger.Error("Freq WEEKLY found, but BYDAY parameter was missing")
				return nil
			}
			repetition.ByDay = matches[4]
			break
		case "MONTHLY":
			if matches[3] != "BYMONTHDAY" {
				logger.Error("Freq MONTHLY found, but BYMONTHDAY parameter was missing")
				return nil
			}
			if byMonthDay, err := strconv.Atoi(matches[4]); err != nil {
				logger.Error("Failed to read BYMONTHDAY parameter")
				return nil
			} else {
				repetition.ByMonthDay = byMonthDay
			}
			break
		default:
			//errors.New("")
			logger.Error("Unknown or unimplemented Freq value found")
			return nil
		}
		if interval, err := strconv.Atoi(matches[5]); err != nil {
			logger.Error("Failed to read INTERVAL parameter")
			return nil
		} else {
			repetition.Interval = interval
		}
		return repetition
	case *string:
		if value == nil {
			return nil
		}
		return unserializeRepetitionRule(*value)
	default:
		return nil
	}
}
