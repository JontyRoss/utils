package datetime

import (
	"strings"
	"time"

	"bitbucket.org/ksenaGroup/utils/logger"
)

//Note:
//This Display format has been superceded by the graphql display time found in the graphqlex package for external communication, and for internal nats
//communication go's native json mashalling of time.Time vars works fine.
//Thus this package can probably be removed eventually.
const DisplayFormat = "2006-01-02 15:04:05"

type FormatTime struct {
	time.Time        // embedded time value
	Format    string // format
}

func (formatTime *FormatTime) UnmarshalJSON(passedByte []byte) error {
	parsedTime, err := time.Parse(formatTime.Format, strings.Replace(
		string(passedByte),
		"\"",
		"",
		-1,
	))

	if err != nil {
		return err
	}
	var newFormatTime FormatTime
	newFormatTime.Time = parsedTime
	*formatTime = newFormatTime

	return nil
}

func (formatTime FormatTime) MarshalJSON() ([]byte, error) {

	if formatTime.Format == "" {
		logger.Debug("Format was empty, using default")
		return []byte(`"` + formatTime.Time.Format(DisplayFormat) + `"`), nil
	}
	logger.Debugf("Format has a value %+v", formatTime.Format)
	return []byte(`"` + formatTime.Time.Format(formatTime.Format) + `"`), nil
}

func Parse(layout, value string) (FormatTime, error) {
	var formatTime FormatTime
	time, err := time.Parse(layout, value)
	formatTime.Time = time
	return formatTime, err
}

func (formatTime FormatTime) SetFormat(value string) {
	formatTime.Format = value
}
